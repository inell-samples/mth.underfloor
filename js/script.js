new Swiper(".hero__slider", {
    slidesPerView: 2,
    loop: true,
    spaceBetween: 10,
    navigation: {
        prevEl: '.hero__slider-btn_prev',
        nextEl: '.hero__slider-btn_next'
    },
    autoplay: {
        delay: 3000
    },
    breakpoints: {
        560: {
            spaceBetween: 8
        },
        320: {
            slidesPerView: 1
        }
    }
});

const tariff = {
    econom: 550,
    comfort: 1400,
    premium: 2700
};

const calcForm = document.querySelector(".js-calc-form");
const totalSquare = document.querySelector(".js-square");
const totalPrice = document.querySelector(".js-total-price");
const resultWrapper = document.querySelector(".calc__result-wrapper");
const calcOrder = document.querySelector(".calc__order");

const submitBtn = document.querySelector(".js-submit");

calcForm.addEventListener("input", () => {
    submitBtn.disabled = !(calcForm.width.value > 0 && calcForm.length.value > 0);
});

calcForm.addEventListener("submit", (event) => {
    event.preventDefault();

    if (calcForm.width.value > 0 && calcForm.length.value > 0) {
        const square = calcForm.width.value * calcForm.length.value;
        const price = square * tariff[calcForm.tariff.value];

        resultWrapper.classList.add("calc__result-wrapper_show");
        calcOrder.classList.add("calc__order_show");

        totalSquare.textContent = `${square} кв.м.`;
        totalPrice.textContent = `${price} руб.`;
    }
});

modalController({
    modal: '.modal',
    btnOpen: '.js-order',
    btnClose: '.modal__close'
});

const phone = document.querySelector("#phone");
const imPhone = new Inputmask("+7(999)999-99-99");
imPhone.mask(phone);

const validator = new JustValidate(".modal__form", {
    errorLabelCssClass: "modal__input_error",
    errorLabelStyle: {
        color: "#ffc700"
    }
});
validator
    .addField("#name", [
        {
            rule: "required",
            errorMessage: "Имя где?!"
        },
        {
            rule: "minLength",
            value: 2,
            errorMessage: "Слышь?!"
        },
        {
            rule: "maxLength",
            value: 20,
            errorMessage: "Ну блин!"
        },

    ])
    .addField("#phone", [
        {
            rule: "required",
            errorMessage: "Куда набрать?!"
        },
        {
            validator: value => {
                const phoneNumber = phone.inputmask.unmaskedvalue();
                return phoneNumber.length == 10;
            },
            errorMessage: "Чет мутное"
        }
    ])
    .onSuccess((event) => {
        const form = event.currentTarget;

        fetch('https://jsonplaceholder.typicode.com/posts', {
            method: 'POST',
            body: JSON.stringify({
                name: form.name.value,
                phone: form.phone.value,
            }),
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            },
        })
            .then((response) => response.json())
            .then((data) => {
                form.reset();
                alert(`Заявка ${data.id}`);
            });
    });